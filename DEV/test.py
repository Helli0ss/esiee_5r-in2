#File Name: test.py
#Written by: Mike GAUDRY

import project

project.print_Hello() #TEST-100: test of the FEAT-100

project.sum_1_to_n() #TEST-300: test of the FEAT-300

project.reverse() #TEST-600: test of the FEAT-600

project.day() #TEST-800: test of the FEAT-800
