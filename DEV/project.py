#File name : project.py
#Written by: Mike GAUDRY

#IMPORT
#FEAT-700 : import for the random function
from random import seed
from random import randint
#END IMPORT


#FUNCTIONS

#FEAT-100: A function to print "Hello World" on the user demand
def print_Hello():
    status = input("Say Hello World ? Yes[yY] or No[nN] ? :")
    if status == 'Yes' or status == 'Y' or status  == 'y':
        print("Hello World")

#FEAT-200: the user gives their name and the function greets them with their name
def greetings():
    name = input("What's your name ? (max. 6 characters): ")
    if len(name) > 6: #BUG-200: set a test to the lenght of the name.
        print("too long, please retry")
        exit()
    print("Hello",name)

#FEAT-300: function that for a number "n" given by the user calculate the sum of the number from 1 to n
def sum_1_to_n():
    n = int(input("Give me a number please : "))
    print("I will calculate the sum in a sec, please wait ...")
    S=((n*(n+1))//2) #BUG-300: change the for-loop into a simple equation.
    print("The sum from 1 to ",n," is : ",S)

#FEAT-400: a function wich provide the sum of 2 numbers
def sum_a_b():
    a = int(input("Give me first number (max. 4 digits): "))
    b = int(input("Give me the second number (max. 4 digits):"))
    if len(str(a)) > 4: #BUG-400: set a test to check the lenght of a
        print("the first number is too big, please retry")
        exit()
    elif len(str(b)) > 4: #BUG-400: set a test to check the lenght of b
        print("the second number is too big, please retry")
        exit()
    print("The sum is :",a+b)

#FEAT-500: a function to test if the string is a palindrome
def palindrome():
    s = input("Enter a word to check if it is a palindrome :")
    if s == s[::-1]:
        print("Yes ",s," is a palindrome.")
    else:
        print("No ",s," is not a palindrome.")

#FEAT-600: a function to print the reverse of a word
def reverse():
    s = input("Enter your word : ")
    print(s[::-1])

#FEAT-700: a function that when called outputs a random number
def random_num():
    seed()
    print(randint(0,99999))

#FEAT-800: a function that given a number 1 to 7 outputs the corespondinf day of the week
def day():
    d = int(input("Give a number from 1 to 7 to have the day related"))
    if d == 1:
        print("Monday")
    elif d == 2:
        print("Tuesday")
    elif d == 3:
        print("Wednesday")
    elif d == 4:
        print("Thursday")
    elif d == 5:
        print("Friday")
    elif d == 6:
        print("Saturday")
    elif d == 7:
        print("Sunday")


#FEAT-900: a function that transforms meters to feet
def meter_to_feet():
    meter = float(input("How many meters ? "))
    feet = meter * 3.28
    print(meter,"meters correspond to ",feet," feets")

#FEAT-1000: a function that counts how many times the letter "a" appears in a phrase that the user provides
def occurences():
    s = input("Give me a phrase (max. 20 character):")
    if len(s) > 20: #BUG-1000: set a test to check the good lenght of the phrase
        print("Too many characters, please retry")
        exit()
    n = s.count("a")
    print("There is ",n," 'a' in your phrase")

#END FUNCTIONS


#MAIN
#print_Hello()
#greetings()
#sum_1_to_n()
#sum_a_b()
#palindrome()
#reverse()
#random_num()
#day()
#meter_to_feet()
#occurences()
#END MAIN


